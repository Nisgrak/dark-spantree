# Dark SpanTree

A companion theme of [Dark Gitlab](https://gitlab.com/vednoc/dark-gitlab) for [SpanTree](https://github.com/tavyandy97/span-tree) chrome extension

## 👀 Preview
![Dark SpanTree preview](./images/preview.png)

## 🚀 Installation

A userstyle extension is required, common ones include:

🎨 Stylus for [Firefox](https://addons.mozilla.org/en-US/firefox/addon/styl-us/), [Chrome](https://chrome.google.com/webstore/detail/stylus/clngdbkpkpeebahjckkjfobafhncgmne) or [Opera](https://addons.opera.com/en-gb/extensions/details/stylus/).<br>
🎨 xStyle for [Firefox](https://addons.mozilla.org/firefox/addon/xstyle/) or [Chrome](https://chrome.google.com/webstore/detail/xstyle/hncgkmhphmncjohllpoleelnibpmccpj).

Then:

📦 [Install Dark Gitlab theme](https://gitlab.com/vednoc/dark-gitlab/raw/master/gitlab.user.styls). Check his repo to config [Dark Gitlab](https://gitlab.com/vednoc/dark-gitlab).<br>
📦 [Install the usercss](https://gitlab.com/Nisgrak/dark-spantree/-/raw/master/dark-spantree.user.css). It supports automatic updates.<br>

## 🎉 Acknowledgements
- Vednoc for [Dark Gitlab](https://gitlab.com/vednoc/dark-gitlab)
- tavyandy97 for [SpanTree](https://github.com/tavyandy97/span-tree)
